#!/bin/bash

echo "Your UID is ${UID}"

TEST_UID=1000

if [[ "${UID}" -ne "${TEST_UID}" ]]
then
  echo "Your UID does not match ${TEST_UID}"
  exit 1
# else
#   echo "You are a vagrant user"
#   exit 0
fi
USERNAME=$(id -un)
if [[ "${?}" -ne 0 ]]
then
  echo "The id command was not successful"
  exit 1
fi
echo "Your username is ${USERNAME}"

UTOTEST="vagrant"
if [[ "${USERNAME}" = "$UTOTEST" ]]; then
  echo "Your username matches ${UTOTEST}"
fi

if [[ "${USERNAME}" != "$UTOTEST" ]]; then
    echo "Your username doesn't match ${UTOTEST}"
    exit 1
fi

exit 0
