#!/bin/bash

#Display user id
echo "Your user id is ${UID}"

#Display username
USERNAME=$(id -un)
echo "Your username is ${USERNAME}"

if [[ "${UID}" -eq 0 ]]
then
  echo 'You are a root user'
else
  echo 'You are not root'
fi
	

